#include <stdio.h>

#include "project.h"

#define DEBUG_PRINT(message) UART_UartPutString(message)

void Hardware_Init();
void AppCallBack(uint32 event, void* eventParam);

int main(void)
{
    Hardware_Init();
    
    UART_UartPutString("\r\n");
    UART_UartPutString("If you are able to read this text the terminal connection is configured correctly.\r\n");
    UART_UartPutString("Start transmitting the characters to see an echo in the terminal.\r\n");
    UART_UartPutString("\r\n");

    for(;;) {
        CyBle_ProcessEvents();
    }
}

void Hardware_Init() {
    CyGlobalIntEnable; /* Enable global interrupts. */
    
    /* Enable UART*/
    UART_Start();
    
    /* Enable BLE*/
    CyBle_Start(AppCallBack);
}

void AppCallBack(uint32 event, void* eventParam) {
    CYBLE_API_RESULT_T apiResult;
    CYBLE_GAPC_ADV_REPORT_T advEvent;
    char buffer[32];
    
    switch (event) {
        case CYBLE_EVT_STACK_ON:
            /* start the scan */
            apiResult = CyBle_GapcStartScan(CYBLE_SCANNING_FAST);
            if (apiResult == CYBLE_ERROR_OK) {
                // success
                DEBUG_PRINT("Bluetooth on!\r\n");
            } else {
                // fail
                DEBUG_PRINT("Fail to turn on bluetooth\r\n");
            }
            break;
        case CYBLE_EVT_GAPC_SCAN_PROGRESS_RESULT:
            /* Cast eventParam to CYBLE_GAPC_ADV_REPORT_T type */
            advEvent = *(CYBLE_GAPC_ADV_REPORT_T *) eventParam;
            
            /* Get MAC address */
            snprintf(buffer, 32, "BLE MAC: %02X:%02X:%02X:%02X:%02X:%02X\r\n", 
                        advEvent.peerBdAddr[5], advEvent.peerBdAddr[4],
                        advEvent.peerBdAddr[3], advEvent.peerBdAddr[2],
                        advEvent.peerBdAddr[1], advEvent.peerBdAddr[0]);
            
            /* Print advertisement packet information */
            switch (advEvent.eventType) {
                case CYBLE_GAPC_CONN_UNDIRECTED_ADV:
                    DEBUG_PRINT("Type: Connectable undirected advertising\r\n");
                    DEBUG_PRINT(buffer);
                    break;
                case CYBLE_GAPC_CONN_DIRECTED_ADV:
                    DEBUG_PRINT("Type: Connectable directed advertising\r\n");
                    DEBUG_PRINT(buffer);
                    break;
                case CYBLE_GAPC_SCAN_UNDIRECTED_ADV:
                    DEBUG_PRINT("Type: Scannable undirected advertising\r\n");
                    DEBUG_PRINT(buffer);
                    break;
                case CYBLE_GAPC_NON_CONN_UNDIRECTED_ADV:
                    DEBUG_PRINT("Type: Non connectable undirected advertising\r\n");
                    DEBUG_PRINT(buffer);
                    break;
                default: 
                    break;
            }
            break;
        case  CYBLE_EVT_GAPC_SCAN_START_STOP:    
            if(CyBle_GetState()==CYBLE_STATE_DISCONNECTED)
            {   
                CyBle_GapcStopScan();
            }
            break;
        default:
            break;
    }    
}

#include "hids.h"
#include "common.h"
#include <stdio.h>

void HidsCallBack(uint32 event, void *eventParam) {
    CYBLE_HIDS_CHAR_VALUE_T *locEventParam = (CYBLE_HIDS_CHAR_VALUE_T *)eventParam;

    char buffer[10];
    snprintf(buffer, 10, "%lx \r\n", event);
    DEBUG_PRINT(buffer);

    switch(event) {
        case CYBLE_EVT_HIDSS_NOTIFICATION_ENABLED:
            DEBUG_PRINT("CYBLE_EVT_HIDSS_NOTIFICATION_ENABLED");
            break;
        case CYBLE_EVT_HIDSS_NOTIFICATION_DISABLED:
            DEBUG_PRINT("CYBLE_EVT_HIDSS_NOTIFICATION_DISABLED");
            break;
        case CYBLE_EVT_HIDSS_BOOT_MODE_ENTER:
            DEBUG_PRINT("CYBLE_EVT_HIDSS_BOOT_MODE_ENTER");
            break;
        case CYBLE_EVT_HIDSS_REPORT_MODE_ENTER:
            DEBUG_PRINT("CYBLE_EVT_HIDSS_REPORT_MODE_ENTER");
            break;
        case CYBLE_EVT_HIDSS_SUSPEND:
            DEBUG_PRINT("CYBLE_EVT_HIDSS_SUSPEND");
            break;
        case CYBLE_EVT_HIDSS_EXIT_SUSPEND:
            DEBUG_PRINT("CYBLE_EVT_HIDSS_EXIT_SUSPEND");
            break;
        case CYBLE_EVT_HIDSS_REPORT_CHAR_WRITE:
            DEBUG_PRINT("CYBLE_EVT_HIDSS_REPORT_CHAR_WRITE");
            break;
        case CYBLE_EVT_HIDSC_NOTIFICATION:
            DEBUG_PRINT("CYBLE_EVT_HIDSC_NOTIFICATION");
            break;
        case CYBLE_EVT_HIDSC_READ_CHAR_RESPONSE:
            DEBUG_PRINT("CYBLE_EVT_HIDSC_READ_CHAR_RESPONSE");
            break;
        case CYBLE_EVT_HIDSC_WRITE_CHAR_RESPONSE:
            DEBUG_PRINT("CYBLE_EVT_HIDSC_WRITE_CHAR_RESPONSE");
            break;
        case CYBLE_EVT_HIDSC_READ_DESCR_RESPONSE:
            DEBUG_PRINT("CYBLE_EVT_HIDSC_READ_DESCR_RESPONSE");
            break;
        case CYBLE_EVT_HIDSC_WRITE_DESCR_RESPONSE:           
            DEBUG_PRINT("CYBLE_EVT_HIDSC_WRITE_DESCR_RESPONSE");
            break;
		default:
			break;
    }
}
void HidsInit(void) {    
    /// Register service specific callback function
    CyBle_HidsRegisterAttrCallback(HidsCallBack);
}
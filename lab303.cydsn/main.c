#include <stdio.h>

#include "project.h"

#define DEBUG_PRINT(message) UART_UartPutString(message)

void Hardware_Init();
void AppCallBack(uint32 event, void* eventParam);

int main(void)
{
    Hardware_Init();
    
    DEBUG_PRINT("\r\n");
    DEBUG_PRINT("s - start scanning\r\n");
    DEBUG_PRINT("p - pause scanning\r\n");
    DEBUG_PRINT("r - restart scanning\r\n");
    DEBUG_PRINT("\r\n");
    
    uint32 ch;
    
    for (;;) {
        /* Get received character or zero if nothing has been received yet */
        ch = UART_UartGetChar();
        
        if (0u != ch) 
            UART_UartPutChar(ch);
        
        if ((ch == 's') || (ch == 'S')) {
            DEBUG_PRINT("\r\nStart scanning\r\n");
            CyBle_Start(AppCallBack);
            break;
        }
    }
    

    for(;;) {
        /* Get received character or zero if nothing has been received yet */
        ch = UART_UartGetChar();
        
        if (0u != ch) {
            UART_UartPutChar(ch);
            
            switch (ch) {
                case 'p':
                case 'P':
                    DEBUG_PRINT("\r\nPause scanning\r\n\r\n");
                    CyBle_Stop();
                    break;
                case 'r':
                case 'R':
                    DEBUG_PRINT("\r\nRestart scanning\r\n");
                    CyBle_Start(AppCallBack);
                    break;
                default:
                    break;
            }
        
        }
        
        CyBle_ProcessEvents();
    }
}

void Hardware_Init() {
    CyGlobalIntEnable; /* Enable global interrupts. */
    
    /* Enable UART*/
    UART_Start();
}

void AppCallBack(uint32 event, void* eventParam) {
    CYBLE_API_RESULT_T apiResult;
    CYBLE_GAPC_ADV_REPORT_T advEvent;
    char buffer[32];
    
    switch (event) {
        case CYBLE_EVT_STACK_ON:
            /* start the scan */
            apiResult = CyBle_GapcStartScan(CYBLE_SCANNING_FAST);
            if (apiResult == CYBLE_ERROR_OK) {
                // success
                DEBUG_PRINT("Bluetooth on!\r\n");
            } else {
                // fail
                DEBUG_PRINT("Fail to turn on bluetooth\r\n");
            }
            break;
        case CYBLE_EVT_GAPC_SCAN_PROGRESS_RESULT:
            /* Cast eventParam to CYBLE_GAPC_ADV_REPORT_T type */
            advEvent = *(CYBLE_GAPC_ADV_REPORT_T *) eventParam;
            
            /* Get MAC address */
            snprintf(buffer, 32, "BLE MAC: %02X:%02X:%02X:%02X:%02X:%02X\r\n", 
                        advEvent.peerBdAddr[5], advEvent.peerBdAddr[4],
                        advEvent.peerBdAddr[3], advEvent.peerBdAddr[2],
                        advEvent.peerBdAddr[1], advEvent.peerBdAddr[0]);
            
            /* Print advertisement packet information */
            switch (advEvent.eventType) {
                case CYBLE_GAPC_CONN_UNDIRECTED_ADV:
                    DEBUG_PRINT("Type: Connectable undirected advertising\r\n");
                    DEBUG_PRINT(buffer);
                    break;
                case CYBLE_GAPC_CONN_DIRECTED_ADV:
                    DEBUG_PRINT("Type: Connectable directed advertising\r\n");
                    DEBUG_PRINT(buffer);
                    break;
                case CYBLE_GAPC_SCAN_UNDIRECTED_ADV:
                    DEBUG_PRINT("Type: Scannable undirected advertising\r\n");
                    DEBUG_PRINT(buffer);
                    break;
                case CYBLE_GAPC_NON_CONN_UNDIRECTED_ADV:
                    DEBUG_PRINT("Type: Non connectable undirected advertising\r\n");
                    DEBUG_PRINT(buffer);
                    break;
                default: 
                    break;
            }
            break;
        case  CYBLE_EVT_GAPC_SCAN_START_STOP:    
            if(CyBle_GetState()==CYBLE_STATE_DISCONNECTED)
            {   
                CyBle_GapcStopScan();
            }
            break;
        default:
            break;
    }    
}

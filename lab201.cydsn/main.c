/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

#define LED_ON                      (0u)
#define LED_OFF                     (1u)

void AppCallBack(uint32 event, void* eventParam);
void Hardware_Init();

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    
    Hardware_Init();

    for(;;) {
        CyBle_ProcessEvents();
    }
}

void Hardware_Init() {
    LED_Blue_Write(LED_OFF);
    LED_Red_Write(LED_OFF);
    LED_Green_Write(LED_OFF);
    
    CyBle_Start(AppCallBack);
}

void AppCallBack(uint32 event, void* eventParam) {
    CYBLE_API_RESULT_T apiResult;
    
    switch (event) {
        case CYBLE_EVT_STACK_ON:
            apiResult = CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
            if (apiResult == CYBLE_ERROR_OK) {   
                LED_Red_Write(LED_ON);
            }
            break;
        default:
            break;
    }
}

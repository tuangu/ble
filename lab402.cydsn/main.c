#include "project.h"
#include "common.h"
#include "hids.h"
#include <stdio.h>

void Hardware_Init();
void AppCallBack(uint32 event, void* eventParam);

int main(void) {

    Hardware_Init();

    for(;;) {
        CyBle_ProcessEvents();
    }
    
    return 0;
}

void Hardware_Init() {
    /// Enable global interrupts. 
    CyGlobalIntEnable;
    
    /// Enable UART
    UART_Start();
    
    /// Enable Bluetooth
    CyBle_Start(AppCallBack);
}

void AppCallBack(uint32 event, void* eventParam) {
    CYBLE_API_RESULT_T apiResult;
    char buffer[10];
    snprintf(buffer, 10, "%lx \r\n", event);
    DEBUG_PRINT(buffer);
    
    switch (event) {
        ///
        /// General Events
        ///
        case CYBLE_EVT_STACK_ON:
            /* start the scan */
            apiResult = CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
            if (apiResult == CYBLE_ERROR_OK) {
                // success
                DEBUG_PRINT("Advertisement started\r\n");
            } else {
                // fail
                DEBUG_PRINT("Error\r\n");
            }
            break;
        case CYBLE_EVT_TIMEOUT:
            DEBUG_PRINT("Timeout\r\n");
            break;
        ///
        /// GAP Events
        ///
        case CYBLE_EVT_GAP_DEVICE_CONNECTED:
            break;
        case CYBLE_EVT_GAPP_ADVERTISEMENT_START_STOP:
            break;
        case CYBLE_EVT_GAP_ENHANCE_CONN_COMPLETE:
            DEBUG_PRINT("Device connected\r\n");
            break;
        case CYBLE_EVT_GAP_CONNECTION_UPDATE_COMPLETE:
            DEBUG_PRINT("GAP connection update complete\r\n");
            break;
        case CYBLE_EVT_GAP_DEVICE_DISCONNECTED:
            DEBUG_PRINT("Device disconected\r\n");
            break;
        ///
        /// GATT Events
        ///
        case CYBLE_EVT_GATT_CONNECT_IND:
            DEBUG_PRINT("GATT connected\r\n");
            DEBUG_PRINT("Initializing HID service\r\n");
            HidsInit();
            break;
        case CYBLE_EVT_GATTS_READ_CHAR_VAL_ACCESS_REQ:
            DEBUG_PRINT("GATT read char value\r\n");
            break;
        case CYBLE_EVT_GATT_DISCONNECT_IND:
            DEBUG_PRINT("GATT disconnected\r\n");
            break;
        ///
        default:
            break;
    }
}

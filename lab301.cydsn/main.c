/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

int main()
{
    uint32 ch;
    
    CyGlobalIntEnable; /* Enable global interrupts. */

    UART_Start();

    UART_UartPutString("\r\n");
    UART_UartPutString("If you are able to read this text the terminal connection is configured correctly.\r\n");
    UART_UartPutString("Start transmitting the characters to see an echo in the terminal.\r\n");
    UART_UartPutString("\r\n");

    for (;;)
    {
        /* Get received character or zero if nothing has been received yet */
        ch = UART_UartGetChar();

        if (0u != ch)
        {
            /* Transmit the data through UART.
            * This functions is blocking and waits until there is a place in
            * the buffer.
            */
            UART_UartPutChar(ch);
            
            if ('\n' == ch)
                UART_UartPutChar('\r');
                
            if ('\r' == ch)
                UART_UartPutChar('\n');
        }
    }
}

/* [] END OF FILE */
